package com.maestro.speechrecognitiontest.utils

import android.content.Context

import java.io.IOException
import java.io.InputStream
import java.nio.charset.Charset

object JSONUtils {
    fun readJSONFromAsset(context: Context, filename: String): String? {
        val json: String
        try {
            val `is` = context.assets.open(filename)
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            json = String(buffer, Charset.forName("UTF-8"))
        } catch (e: IOException) {
            return null
        }

        return json
    }
}
