package com.maestro.speechrecognitiontest

import android.Manifest
import android.annotation.TargetApi
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.gson.Gson
import com.maestro.speechrecognitiontest.model.Level
import com.maestro.speechrecognitiontest.translation_engine.TranslatorFactory
import com.maestro.speechrecognitiontest.translation_engine.utils.ConversionCallaback
import com.maestro.speechrecognitiontest.utils.JSONUtils
import kotlinx.android.synthetic.main.activity_main.*
import me.xdrop.fuzzywuzzy.FuzzySearch
import java.util.*

const val RESULT_SPEECH: Int = 0

class MainActivity : AppCompatActivity(), ConversionCallaback {
  private val testString = "he wasn't jumping up and down about it"
  private lateinit var level: Level
  private var counter = -1

  override fun onSuccess(result: ArrayList<String>?) {
    if (result != null) {
      var similarity = 0
      for (i in result.indices) {
        val recognizedPhrase = result[i]
        val currentSimilatity = FuzzySearch.weightedRatio(recognizedPhrase, level.phrases!![counter].text)
        if (currentSimilatity > similarity) similarity = currentSimilatity
      }
      recognized_phrase.text = result.joinToString("\n")
      similarity_tv.text = similarity.toString()
      if (similarity > 90) showNextPhrase()
      startRecognize()
    }
  }

  override fun onCompletion() {
    Toast.makeText(this, "Done ", Toast.LENGTH_SHORT).show()
  }

  override fun onError(errorMessage: String) {
    Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show()
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    initLevel()
    if (Build.VERSION.SDK_INT >= 23) {
      requestForPermission()
    } else {
      setUpView()
    }
  }

  private fun initLevel() {
    val json = JSONUtils.readJSONFromAsset(this, "Level_1.json")
    if (json != null) {
      level = Gson().fromJson<Level>(json, Level::class.java)
      showNextPhrase()
    }
  }

  private fun showNextPhrase() {
    counter++
    if (counter == level.phrases!!.size) counter = 0
    initial_phrase.text = level.phrases?.get(counter)?.text
    recognized_phrase.text = ""
    similarity_tv.text = ""
  }

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    super.onActivityResult(requestCode, resultCode, data)
    when (requestCode) {
      RESULT_SPEECH -> {
        if (resultCode == Activity.RESULT_OK && null != data) {
          val text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
          val similarity = FuzzySearch.ratio(text[0], testString)
          Toast.makeText(this, similarity.toString(), Toast.LENGTH_LONG).show()
        }
      }
    }
  }

  /**
   * Set up listeners on View
   */
  private fun setUpView() {
    //Listen and convert convert speech to text
    recognize.setOnClickListener { view -> startRecognize() }

    next.setOnClickListener {
      showNextPhrase()
    }
  }

  fun startRecognize() {
    //Ask translator factory to start speech tpo text convertion
    //Hello There is optional
    TranslatorFactory.getInstance()
        .getTranslator(TranslatorFactory.TRANSLATOR_TYPE.SPEECH_TO_TEXT, this@MainActivity)
        .initialize("Hello There", this@MainActivity)

    MainActivity.CURRENT_MODE = MainActivity.STT
  }

  /**
   * Request Permission
   */
  @TargetApi(Build.VERSION_CODES.M)
  private fun requestForPermission() {
    val permissionsNeeded = ArrayList<String>()
    val permissionsList = ArrayList<String>()
    if (!isPermissionGranted(permissionsList, Manifest.permission.RECORD_AUDIO))
      permissionsNeeded.add("Require for Speech to text")
    if (permissionsList.size > 0) {
      if (permissionsNeeded.size > 0) {
        var message = "You need to grant access to " + permissionsNeeded[0]
        for (i in 1 until permissionsNeeded.size) {
          message = message + ", " + permissionsNeeded[i]
        }
        showMessageOKCancel(message,
            DialogInterface.OnClickListener { dialog, which ->
              requestPermissions(permissionsList.toTypedArray(),
                  REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
            })
        return
      }
      requestPermissions(permissionsList.toTypedArray(),
          REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS)
      return
    }
    //add listeners on view
    setUpView()
  }

  private fun showMessageOKCancel(message: String, okListener: DialogInterface.OnClickListener) {
    AlertDialog.Builder(this@MainActivity)
        .setMessage(message)
        .setPositiveButton("OK", okListener)
        .setNegativeButton("Cancel", null)
        .create()
        .show()
  }

  @TargetApi(Build.VERSION_CODES.M)
  private fun isPermissionGranted(permissionsList: MutableList<String>, permission: String): Boolean {

    if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
      permissionsList.add(permission)

      // Check for Rationale Option
      if (!shouldShowRequestPermissionRationale(permission))
        return false
    }
    return true
  }

  override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
    when (requestCode) {
      REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS -> {
        val perms = HashMap<String, Int>()
        perms[Manifest.permission.RECORD_AUDIO] = PackageManager.PERMISSION_GRANTED
        for (i in permissions.indices)
          perms[permissions[i]] = grantResults[i]
        if (perms[Manifest.permission.RECORD_AUDIO] == PackageManager.PERMISSION_GRANTED) {
          //add listeners on view
          setUpView()
        } else {
          Toast.makeText(this@MainActivity, "Some Permissions are Denied Exiting App", Toast.LENGTH_SHORT)
              .show()
          finish()
        }
      }
      else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }
  }

  companion object {
    private val STT = 1
    private var CURRENT_MODE = -1
    private val REQUEST_CODE_ASK_MULTIPLE_PERMISSIONS = 124
  }
}
