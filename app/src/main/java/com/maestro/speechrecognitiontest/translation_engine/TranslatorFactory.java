package com.maestro.speechrecognitiontest.translation_engine;

import com.maestro.speechrecognitiontest.translation_engine.translators.IConverter;
import com.maestro.speechrecognitiontest.translation_engine.translators.SpeechToTextConverter;
import com.maestro.speechrecognitiontest.translation_engine.translators.TextToSpeechConverter;
import com.maestro.speechrecognitiontest.translation_engine.utils.ConversionCallaback;

/**
 * This Factory class return object of TTS or STT dependending on input enum TRANSLATOR_TYPE
 */
public class TranslatorFactory {

    private static TranslatorFactory ourInstance = new TranslatorFactory();

    public enum TRANSLATOR_TYPE {TEXT_TO_SPEECH, SPEECH_TO_TEXT}

    private TranslatorFactory() {
    }

    public static TranslatorFactory getInstance() {
        return ourInstance;
    }

    /**
     * Factory method to return object of STT or TTS
     *
     * @param translator_type
     * @param conversionCallaback
     * @return
     */
    public IConverter getTranslator(TRANSLATOR_TYPE translator_type, ConversionCallaback conversionCallaback) {
        switch (translator_type) {
            case TEXT_TO_SPEECH:

                //Get Text to speech translator
                return new TextToSpeechConverter(conversionCallaback);

            case SPEECH_TO_TEXT:

                //Get speech to text translator
                return new SpeechToTextConverter(conversionCallaback);
        }

        return null;
    }
}
