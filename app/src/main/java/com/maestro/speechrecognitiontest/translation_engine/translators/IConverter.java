package com.maestro.speechrecognitiontest.translation_engine.translators;

import android.app.Activity;

public interface IConverter {
    IConverter initialize(String message, Activity appContext);
}
