package com.maestro.speechrecognitiontest.translation_engine.utils

import java.util.ArrayList

interface ConversionCallaback {
    fun onSuccess(result: ArrayList<String>?)
    fun onCompletion()
    fun onError(errorMessage: String)
} 