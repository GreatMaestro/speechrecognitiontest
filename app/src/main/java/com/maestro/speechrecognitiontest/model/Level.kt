package com.maestro.speechrecognitiontest.model

import com.google.gson.annotations.SerializedName

import java.util.ArrayList

class Level {
    @SerializedName("number")
    internal var id: Int = 0
    @SerializedName("level")
    internal var name: String? = null

    @SerializedName("phrases")
    var phrases: ArrayList<Phrase>? = null

    inner class Phrase {
        @SerializedName("id")
        var id: Int = 0
        @SerializedName("text")
        var text: String? = null
        @SerializedName("ru")
        var ru: String? = null
    }
}
